/*
 * Name:      Kristen Calistro
 *
 * Course:    CSCI-13, Spring 2017
 *
 * Date:      5/17/17
 *
 * File Name: ZodiacController.java
 *
 * Purpose:   Controller piece of the M/V/C paradigm that instantiates the controls of the view
 *            and transfers information from the ZodiacModel to the view to be displayed for the user
 *            when the user presses a button.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class ZodiacController implements Initializable{
   
   /*
    * declare variables for each controller
    */
   
   @FXML
   private TextField txtMonth;
   
    @FXML
   private TextField txtDay;
   
   @FXML
   private Button btnGetSign;  
   
   @FXML
   private Label lblSignName;
   
   @FXML
   private Label lblSignInfo;
   
   @FXML
   private ImageView iconSign;
   
   @FXML
   private void handleButtonAction(ActionEvent e) {
     // access model by creating object of ZodiacModel class
     ZodiacModel zodiac = new ZodiacModel();

     // Check that get sign button has been pressed
     if (e.getSource() == btnGetSign)
     { 
       // check that user input is a numerical month and day
       if (zodiac.isNumeric(txtMonth.getText()) == true && zodiac.isNumeric(txtDay.getText()) == true){
         int month = Integer.parseInt(txtMonth.getText());
         int day = Integer.parseInt(txtDay.getText());
         
         // if the month and day are valid get the zodiac name, info and image 
         // from ZodiacModel methods and set each controller
         if (zodiac.isValid(month, day) == true) 
         {
           lblSignName.setText(zodiac.getSignName(month, day));
           lblSignInfo.setText(zodiac.getSignInfo(month, day));
           iconSign.setImage(zodiac.getSignImage(month, day));
         }
         // if month and/or day is invalid, display error message and error image
         else  
         {
           lblSignInfo.setText("Invalid month and/or day");
           lblSignName.setText("");
           iconSign.setImage(new Image("./error.png"));
         }
       }
       // if the month and/or day are not numerical, display error message and error image
       else{
         lblSignInfo.setText("Invalid month and/or day. Month and Day must be numerical.");
         lblSignName.setText("");
         iconSign.setImage(new Image("./error.png"));
       }    
     }
   }
   
   @Override
      public void initialize(URL url, ResourceBundle rb) {
      // TODO
   }
   
}