/*
 * Name:      Kristen Calistro
 *
 * Course:    CSCI-13, Spring 2017
 *
 * Date:      5/17/17
 *
 * File Name: ZodiacMain.java
 *
 * Purpose:   Initiates the view and controller
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ZodiacMain extends Application{

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("./ZodiacView.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("CSCI 13 Final Project");
        stage.show();
    }
  
    public static void main(String[] args) {
        launch(args);
    }
} 