/*
 * Name:      Kristen Calistro
 *
 * Course:    CSCI-13, Spring 2017
 *
 * Date:      5/17/17
 *
 * File Name: ZodiacModelTest.java
 *
 * Purpose:   JUnit tests to test all the methods of the ZodiacModel class
 */

import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


public class ZodiacModelTest {


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }

    /** Test isNumeric method 
     *  of ZodiacModel. **/
    @Test
    public void testIsNumeric1(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals(true, sign.isNumeric("3"));
    }
    
    @Test
    public void testIsNumeric2(){
        ZodiacModel sign = new ZodiacModel(); 
        assertEquals(false, sign.isNumeric("String"));
    }
    

    /** Test isValid method 
     *  of ZodiacModel. **/
    
    @Test
    public void testValid1(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals(true, sign.isValid(1, 20));
    }
    
    @Test
    public void testValid2(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals(false, sign.isValid(2, 30));
    }
    
    /** Test getSignName method of ZodiacModel
     *  for each of the 12 possible signs. **/
     
    @Test
    public void testGetName1(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Capricorn", sign.getSignName(12, 25));
    } 

    @Test
    public void testGetName2(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Aquarius", sign.getSignName(2, 4));
    }
    
    @Test
    public void testGetName3(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Pisces", sign.getSignName(3, 13));
    }
    
    @Test
    public void testGetName4(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Aries", sign.getSignName(4, 10));
    }
    
    @Test
    public void testGetName5(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Taurus", sign.getSignName(4, 20));
    }
    
    @Test
    public void testGetName6(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Gemini", sign.getSignName(5, 28));
    }
    
    @Test
    public void testGetName7(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Cancer", sign.getSignName(7, 4));
    }
    
    @Test
    public void testGetName8(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Leo", sign.getSignName(7, 26));
    }
    
    @Test
    public void testGetName9(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Virgo", sign.getSignName(9, 11));
    }
    
    @Test
    public void testGetName10(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Libra", sign.getSignName(10, 3));
    }
    
    @Test
    public void testGetName11(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Scorpio", sign.getSignName(10, 30));
    }
    
    @Test
    public void testGetName12(){
        ZodiacModel sign = new ZodiacModel();
        assertEquals("Sagittarius", sign.getSignName(11, 29));
    }

    /** Test getSignInfo method of ZodiacModel
     *  for each of the 12 possible signs. **/

    @Test
    public void testGetSignInfo1(){
        ZodiacModel capricorn = new ZodiacModel();
        assertEquals("Capricorn sign knows what she wants and has good manners.\n" + 
                     "Capricorn sign adores success and power and does not\n" + 
                     "allow emotions to blind him. A person of the Capricorn sign\n" + 
                     "may fall ill because of anxiety. A Capricorn woman loves beauty\n" +
                     "and A Capricorn man is strong with a gentle heart.", capricorn.getSignInfo(12, 25));
    }
    
    @Test
    public void testGetSignInfo2(){
        ZodiacModel aquarius = new ZodiacModel();
        assertEquals("The key feature of Aquarius sign is curiosity and the ability\n" + 
                     "of deep insight. Aquarius sign is the sign of geniuses. Aquarius\n" +
                     "sign is contradictory in love. Aquarius sign has a very sensitive\n" +
                     "nervous system. Aquarius woman belongs to all and nobody.\n" +
                     "Aquarius man may be a good friend and clever adviser.", aquarius.getSignInfo(1, 30));
    }
    
    @Test
    public void testGetSignInfo3(){
        ZodiacModel pisces = new ZodiacModel();
        assertEquals("Pisces are compassionate and gentle and often put their frieds\n" +
                     "needs before their own. Pisces are very artistic and are more\n" +
                     "intuitive than others. Individuals of the pisces sign are\n" +
                     "hopeless romantics. Pisces woman will inspire and lift up her\n" +
                     "loved ones. Pisces man is caring, tender and sensitive.", pisces.getSignInfo(3, 5));
    }
    
    @Test
    public void testGetSignInfo4(){
        ZodiacModel aries = new ZodiacModel();
        assertEquals("Aries-born are open-hearted and strong, and their main attributes\n" +
                     "are frankness and honesty. Aries sign loves work and is rarely ill.\n" +
                     "Aries sign is true and fair in love. Aries woman is intellectual\n" +
                     "and extremely strong. Aries man is overflown with ideas and\n" + 
                     "creative energy.", aries.getSignInfo(3, 29));
    }
    
    @Test
    public void testGetSignInfo5(){
        ZodiacModel taurus = new ZodiacModel();
        assertEquals("Taurus-born are stable and conservative, making them one of the\n" +
                     "most reliable signs of the Zodiac. They see things from a grounded\n" +
                     "and realistic perspective. Taurus sign finds it easy to make money\n" +
                     "and will do whatever it takes to complete a task they've started.\n" +
                     "Taurus man is complicated and has a soft emotional side. Taurus\n" +
                     "woman is loyal and ready to settle down when she finds true love.", taurus.getSignInfo(5, 9));
    }
    
    @Test
    public void testGetSignInfo6(){
        ZodiacModel gemini = new ZodiacModel();
        assertEquals("Individuals of the Gemini sign are curious, affectionate and\n" +
                     "adaptable, but can also be indecisive. They have a thirst for\n" +
                     "knowledge and have the ability to think clearly and communicate\n" +
                     "effictively. Gemini man is positive, adventurous and surprising, and\n" +
                     "when he is in true love, he shows his pure childlike heart. Gemini\n" +
                     "woman yearns for tenderness given in an oddly distant way and\n" +
                     "needs to feel things deeply beyond her mind", gemini.getSignInfo(6, 10));
    }
    
    @Test
    public void testGetSignInfo7(){
        ZodiacModel cancer = new ZodiacModel();
        assertEquals("Cancer-born are very loyal, sympathetic and affectionate, and they\n" +
                     "care deeply about family matters and the home. They are very \n" +
                     "generous and quick to help others, but are just as quick to avoid \n" + 
                     "conflict. Cancer man is very affectionate and dominated by emotion  \n" +
                     "and matters of the heart. Cancer woman likes to hold on to things that\n" +
                     "make her happy and she wont change her life easily." , cancer.getSignInfo(7, 1));
    }
    
    @Test
    public void testGetSignInfo8(){
        ZodiacModel leo = new ZodiacModel();
        assertEquals("Leo sign is represented by the lion, and just like a lion, Leo-born\n" +
                     "are great leaders. Because Leos are generous and loyal, they often\n" +
                     "have many friends. While a Leo man is open hearted and \n" +
                     "comfortable to be with, he can also be bossy and self-involved. A \n" +
                     "Leo woman is self confident, and can be inspiring to others in the\n" +
                     "fact that she believes there is nothing that can't be done.", leo.getSignInfo(8, 5));
    }
    
    @Test
    public void testGetSignInfo9(){
        ZodiacModel virgo = new ZodiacModel();
        assertEquals("Virgo-born are hardowrking and analytical, paying attention to the\n" +
                     "smallest detail. They can be overly critical of themselves and\n" + 
                     "others due to getting stuck in details. Virgo man is eager to fix\n" +
                     "something and won't rest untill he does. Virgo woman wants to help\n" +
                     "and get invloved, but can sometimes become overbearing.", virgo.getSignInfo(9, 8));
    }
    
    @Test
    public void testGetSignInfo10(){
        ZodiacModel libra = new ZodiacModel();
        assertEquals("Libra-born are fair-minded and social, and hate being alone.\n" +
                     "They want to keep peace and avoid all tyes of conflict when possible\n" +
                     "Libras value quality over quantity and love beautiful things.\n" +
                     "Libra woman cares to much about others opinions of her due to her\n" +
                     "low self-esteem, but is also very caring and loyal. Libra man is\n" + 
                     "indecisive because he is very attentive to consequences that might\n" +
                     "follow each decision.", libra.getSignInfo(10, 10));
    }
    
    @Test
    public void testGetSignInfo11(){
        ZodiacModel scorpio = new ZodiacModel();
        assertEquals("Scorpio-born are resourceful and passionate and are persistent in\n" +
                     "searching for the truth. Scorpios are brave and have a lot of friends.\n" +
                     "Because Scorpio woman loves with her entire heart, she can be \n" + 
                     "deeply sensitive, scared of betrayal, and often hurt. Scorpio man \n" + 
                     "is loyal,giving and emotional, but can also be possesive and jealous\n " +
                     "at times.", scorpio.getSignInfo(11, 21));
    }
    
    @Test
    public void testGetSignInfo12(){
        ZodiacModel sagittarius = new ZodiacModel();
        assertEquals("Sagittarius-born are philosophical and always searching for the\n" +
                     "meaning of life. They are big travelers and need to feel constantly in\n" +
                     "touch with the world around them. Sagittarius man is optimistic, but\n" +
                     "can also be unreliable and lose focus easily. Sagittarius woman's \n" +
                     "mission is to make the world a better place.", sagittarius.getSignInfo(12, 15));
    }
}
