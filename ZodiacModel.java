/*
 * Name:      Kristen Calistro
 *
 * Course:    CSCI-13, Spring 2017
 *
 * Date:      5/17/17
 *
 * File Name: ZodiacModel.java
 *
 * Purpose:   Model to hold all the Zodiac Sign data within methods for the M/V/C paradigm.
 */
 
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javafx.scene.image.Image;

public class ZodiacModel{
    
    // boolean method to check that a string is numeric
    public static boolean isNumeric(String str){  
        try  
        {  
            int d = Integer.parseInt(str);  
        }  
        catch(java.lang.NumberFormatException nfe)  
        {  
            return false;  
        }  
        return true;  
    }
    
    // boolean method to check that the month and day entered are valid
    public boolean isValid(int month, int day){
        if ((month == 1 || month == 3 || month == 5 || month == 7 || 
            month ==8 || month == 10 || month == 12) && (day >=1 && day <= 31))
            return true;
        if (month == 2 && (day >=1 && day <= 29))
            return true;
        if ((month == 4 || month == 6 || month == 9 || month == 11) && (day >=1 && day <= 30))
            return true; 
        else 
            return false; 
    }
    
    // method to return a Zodiac sign name given a specific month and day
    public String getSignName(int month, int day){
        if      ((month == 12 && day >= 22 && day <= 31) || (month ==  1 && day >= 1 && day <= 19))
            return "Capricorn";
        else if ((month ==  1 && day >= 20 && day <= 31) || (month ==  2 && day >= 1 && day <= 17))
            return "Aquarius";
        else if ((month ==  2 && day >= 18 && day <= 29) || (month ==  3 && day >= 1 && day <= 19))
            return "Pisces";
        else if ((month ==  3 && day >= 20 && day <= 31) || (month ==  4 && day >= 1 && day <= 19))
            return "Aries";
        else if ((month ==  4 && day >= 20 && day <= 30) || (month ==  5 && day >= 1 && day <= 20))
            return "Taurus";
        else if ((month ==  5 && day >= 21 && day <= 31) || (month ==  6 && day >= 1 && day <= 20))
            return "Gemini";
        else if ((month ==  6 && day >= 21 && day <= 30) || (month ==  7 && day >= 1 && day <= 22))
            return "Cancer";
        else if ((month ==  7 && day >= 23 && day <= 31) || (month ==  8 && day >= 1 && day <= 22))
            return "Leo";
        else if ((month ==  8 && day >= 23 && day <= 31) || (month ==  9 && day >= 1 && day <= 22))
            return "Virgo";
        else if ((month ==  9 && day >= 23 && day <= 30) || (month == 10 && day >= 1 && day <= 22))
            return "Libra";
        else if ((month == 10 && day >= 23 && day <= 31) || (month == 11 && day >= 1 && day <= 21))
            return "Scorpio";
        else if ((month == 11 && day >= 22 && day <= 30) || (month == 12 && day >= 1 && day <= 21))
            return "Sagittarius"; 
        return null;
    }
    
    // method to return information about a Zodiac Sign given a month and day to match the sign
    public String getSignInfo(int month, int day){
        String sign = getSignName(month, day);
        if (sign == "Capricorn")
            return "Capricorn sign knows what she wants and has good manners.\n" + 
                   "Capricorn sign adores success and power and does not\n" + 
                   "allow emotions to blind him. A person of the Capricorn sign\n" + 
                   "may fall ill because of anxiety. A Capricorn woman loves beauty\n" +
                   "and A Capricorn man is strong with a gentle heart.";
       
        if (sign == "Aquarius")
            return "The key feature of Aquarius sign is curiosity and the ability\n" + 
                   "of deep insight. Aquarius sign is the sign of geniuses. Aquarius\n" +
                   "sign is contradictory in love. Aquarius sign has a very sensitive\n" +
                   "nervous system. Aquarius woman belongs to all and nobody.\n" +
                   "Aquarius man may be a good friend and clever adviser.";
        if (sign == "Pisces")
            return "Pisces are compassionate and gentle and often put their frieds\n" +
                   "needs before their own. Pisces are very artistic and are more\n" +
                   "intuitive than others. Individuals of the pisces sign are\n" +
                   "hopeless romantics. Pisces woman will inspire and lift up her\n" +
                   "loved ones. Pisces man is caring, tender and sensitive.";
        if (sign == "Aries")
            return "Aries-born are open-hearted and strong, and their main attributes\n" +
                   "are frankness and honesty. Aries sign loves work and is rarely ill.\n" +
                   "Aries sign is true and fair in love. Aries woman is intellectual\n" +
                   "and extremely strong. Aries man is overflown with ideas and\n" + 
                   "creative energy.";
        if (sign == "Taurus")
            return "Taurus-born are stable and conservative, making them one of the\n" +
                   "most reliable signs of the Zodiac. They see things from a grounded\n" +
                   "and realistic perspective. Taurus sign finds it easy to make money\n" +
                   "and will do whatever it takes to complete a task they've started.\n" +
                   "Taurus man is complicated and has a soft emotional side. Taurus\n" +
                   "woman is loyal and ready to settle down when she finds true love.";
        if (sign == "Gemini")
            return "Individuals of the Gemini sign are curious, affectionate and\n" +
                   "adaptable, but can also be indecisive. They have a thirst for\n" +
                   "knowledge and have the ability to think clearly and communicate\n" +
                   "effictively. Gemini man is positive, adventurous and surprising, and\n" +
                   "when he is in true love, he shows his pure childlike heart. Gemini\n" +
                   "woman yearns for tenderness given in an oddly distant way and\n" +
                   "needs to feel things deeply beyond her mind";
        if (sign == "Cancer")
            return "Cancer-born are very loyal, sympathetic and affectionate, and they\n" +
                   "care deeply about family matters and the home. They are very \n" +
                   "generous and quick to help others, but are just as quick to avoid \n" + 
                   "conflict. Cancer man is very affectionate and dominated by emotion  \n" +
                   "and matters of the heart. Cancer woman likes to hold on to things that\n" +
                   "make her happy and she wont change her life easily." ;
        if (sign == "Leo")
            return "Leo sign is represented by the lion, and just like a lion, Leo-born\n" +
                   "are great leaders. Because Leos are generous and loyal, they often\n" +
                   "have many friends. While a Leo man is open hearted and \n" +
                   "comfortable to be with, he can also be bossy and self-involved. A \n" +
                   "Leo woman is self confident, and can be inspiring to others in the\n" +
                   "fact that she believes there is nothing that can't be done.";
        if (sign == "Virgo")
            return "Virgo-born are hardowrking and analytical, paying attention to the\n" +
                   "smallest detail. They can be overly critical of themselves and\n" + 
                   "others due to getting stuck in details. Virgo man is eager to fix\n" +
                   "something and won't rest untill he does. Virgo woman wants to help\n" +
                   "and get invloved, but can sometimes become overbearing.";
        if (sign == "Libra")
            return "Libra-born are fair-minded and social, and hate being alone.\n" +
                   "They want to keep peace and avoid all tyes of conflict when possible\n" +
                   "Libras value quality over quantity and love beautiful things.\n" +
                   "Libra woman cares to much about others opinions of her due to her\n" +
                   "low self-esteem, but is also very caring and loyal. Libra man is\n" + 
                   "indecisive because he is very attentive to consequences that might\n" +
                   "follow each decision.";
        if (sign == "Scorpio")
            return "Scorpio-born are resourceful and passionate and are persistent in\n" +
                   "searching for the truth. Scorpios are brave and have a lot of friends.\n" +
                   "Because Scorpio woman loves with her entire heart, she can be \n" + 
                   "deeply sensitive, scared of betrayal, and often hurt. Scorpio man \n" + 
                   "is loyal,giving and emotional, but can also be possesive and jealous\n " +
                   "at times.";
        if (sign == "Sagittarius")
            return "Sagittarius-born are philosophical and always searching for the\n" +
                   "meaning of life. They are big travelers and need to feel constantly in\n" +
                   "touch with the world around them. Sagittarius man is optimistic, but\n" +
                   "can also be unreliable and lose focus easily. Sagittarius woman's \n" +
                   "mission is to make the world a better place.";
        return null;
    }
    
    // method to return an image that represents the sign matching the month and day given
    public Image getSignImage(int month, int day){
        String sign = getSignName(month, day);
        
        if (sign == "Capricorn")
            return new Image("./capricorn.png");
        if (sign == "Aquarius")
            return new Image("./aquarius.png");
        if (sign == "Pisces")
            return new Image("./pisces.png");
        if (sign == "Aries")
            return new Image("./aries.png");
        if (sign == "Taurus")
            return new Image("./taurus.png");
        if (sign == "Gemini")
            return new Image("./gemini.png");
        if (sign == "Cancer")
            return new Image("./cancer.png");
        if (sign == "Leo")
            return new Image("./leo.png");
        if (sign == "Virgo")
            return new Image("./virgo.png");
        if (sign == "Libra")
            return new Image("./libra.png");
        if (sign == "Scorpio")
            return new Image("./scorpio.png");
        if (sign == "Sagittarius")
            return new Image("./sagittarius.png");
        return null;
    }
}